I18N message resolver service
=============================

This repository implements a very simple service that exposes an endpoint on the ELG platform at `/dev/i18n/resolve` (replace `/dev` as appropriate) that is able to render status message JSON objects received from LT services into plain text messages in the caller's language.  The endpoint accepts a POST with `Content-Type: application/json` whose content is a JSON array of zero or more status message objects:

```json
[
  {
    "code":"elg.service.internalError",
    "text":"Internal error during processing: {0}",
    "params":["some error message"]
  }
]
```

and returns a matching array of rendered message strings.  The ELG provides a set of standard message `code` values with translations into other languages, the message translations and the fallback `text` may contain numbered placeholders `{0}`, `{1}`, etc. which are filled in by the corresponding index in the `params` array.

By default the resolver chooses the language to use based on the `Accept-Language` header sent by the calling browser, but this can be overridden by providing a query parameter in the post URL, e.g. `?lang=de`.  It looks up the message code in the list of standard messages, and if found it uses the translated format string for that code.  If the code is not found, or if there is no translation for that code in the relevant language, then it will fall back to use the `text` supplied in the original JSON.  Finally it will fill in any placeholders in the format string using the `params` from the message, and return the result.

## Adding more messages

The resolver reads its messages from standard Java `ResourceBundle` style properties files named `eu/elg/elg-messages*.properties`.  At startup it scans a base folder (in the Docker image this is `/sources`) for sub-folders and/or JAR files that contain message files at this path.  In the normal `ResourceBundle` way, when looking up a message code in a locale of `lang-COUNTRY` the resolver will first look in `elg-messages_lang-COUNTRY.properties`, then `elg-messages_lang.properties` and finally `elg-messages.properties`, using the first match it finds.  So for example for `de-AT` it will first try `elg-messages_de_AT.properties`, then `elg-messages_de.properties`, etc.

To add a new set of message codes simply add (or bind-mount) a new directory or JAR file into `/sources` containing property files in the right locations.  For JAR files published to Maven Central this can be done at build time by adding an `i18n` entry to the `dependencies` block in `build.gradle`.