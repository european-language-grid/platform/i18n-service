package eu.elg.i18n;

import io.micronaut.http.HttpRequest;
import io.micronaut.http.server.util.locale.HttpAbstractLocaleResolver;
import io.micronaut.http.server.util.locale.HttpLocaleResolutionConfiguration;
import jakarta.inject.Singleton;

import java.util.Locale;
import java.util.Optional;

@Singleton
public class QueryParamLocaleResolver extends HttpAbstractLocaleResolver {

  public QueryParamLocaleResolver(HttpLocaleResolutionConfiguration conf) {
    super(conf);
  }

  @Override
  public Optional<Locale> resolve(HttpRequest<?> context) {
    return context.getParameters().get("lang", Locale.class);
  }

}