package eu.elg.i18n;

import eu.elg.model.StatusMessage;
import io.micronaut.core.annotation.Introspected;

/**
 * Generate introspection data for the StatusMessage class so it can be properly deserialized under graalvm.
 */
@Introspected(classes = StatusMessage.class)
public class ElgBindingsIntrospection {
}
