package eu.elg.i18n;

import eu.elg.model.StatusMessage;
import io.micronaut.caffeine.cache.Caffeine;
import io.micronaut.caffeine.cache.LoadingCache;
import io.micronaut.context.annotation.Value;
import io.micronaut.core.annotation.TypeHint;
import jakarta.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.MessageFormat;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.stream.Collectors;

// hint the types that Caffeine builder needs to load by reflection
@TypeHint(typeNames = {"io.micronaut.caffeine.cache.SSMS", "io.micronaut.caffeine.cache.PSMS"})
@Singleton
public class MessageResolver {
  private static final Logger log = LoggerFactory.getLogger(MessageResolver.class);

  private final Map<String, Map<String, ThreadLocal<MessageFormat>>> messages = new HashMap<>();

  private final ResourceBundle.Control rbControl = ResourceBundle.Control.getControl(ResourceBundle.Control.FORMAT_DEFAULT);

  private final LoadingCache<Object, ThreadLocal<MessageFormat>> textCache = Caffeine.newBuilder()
          .maximumSize(500).build((v) -> ThreadLocal.withInitial(() -> new MessageFormat((String)v)));

  private final LoadingCache<Object, List<String>> localeCache = Caffeine.newBuilder()
          .maximumSize(500).build((l) -> {
            List<String> candidates = rbControl.getCandidateLocales("", (Locale)l).stream()
                    .map(loc -> rbControl.toBundleName("", loc))
                    .filter(messages::containsKey).collect(Collectors.toList());
            log.info("Generated candidates for locale {}: {}", l, candidates);

            return candidates;
          });

  public MessageResolver(@Value("${elg.i18n.base-dir}") Path baseDir) throws IOException {
    Files.list(baseDir).filter(p -> Files.isDirectory(p) || p.toString().endsWith(".jar"))
            .forEach(p -> {
              if(Files.isDirectory(p)) {
                try(var s = Files.newDirectoryStream(p.resolve("eu/elg"), "elg-messages*.properties")) {
                  for(var propsPath : s) {
                    Properties props = new Properties();
                    String filename = propsPath.getFileName().toString();
                    String langCode = filename.substring(12, filename.length() - 11); // 12 = elg-messages, 11 = .properties
                    Locale locale = "".equals(langCode) ? Locale.ROOT : Locale.forLanguageTag(langCode.substring(1).replaceAll("_.*$", ""));
                    log.info("Processing {}, language code \"{}\"", propsPath, langCode);
                    Map<String, ThreadLocal<MessageFormat>> thisLangMessages = messages.computeIfAbsent(langCode, (l) -> new HashMap<>());
                    try(InputStream stream = Files.newInputStream(propsPath)) {
                      props.load(stream);
                    }
                    for(Map.Entry<Object, Object> entry : props.entrySet()) {
                      String format = (String)entry.getValue();
                      thisLangMessages.put((String) entry.getKey(), ThreadLocal.withInitial(() -> new MessageFormat(format, locale)));
                    }
                  }
                } catch(IOException e) {
                  log.warn("Could not load properties from {}", p);
                }
              } else {
                // jar file
                try {
                  JarFile f = new JarFile(p.toFile());
                  for(var e : (Iterable<JarEntry>)() -> f.stream().filter(e -> e.getName().startsWith("eu/elg/elg-messages") && e.getName().endsWith(".properties")).iterator()) {
                    Properties props = new Properties();
                    String filename = e.getName();
                    String langCode = filename.substring(19, filename.length() - 11); // 19 = eu/elg/elg-messages, 11 = .properties
                    Locale locale = "".equals(langCode) ? Locale.ROOT : Locale.forLanguageTag(langCode.substring(1).replaceAll("_.*$", ""));
                    log.info("Processing {} in {}, language code \"{}\"", filename, p, langCode);
                    Map<String, ThreadLocal<MessageFormat>> thisLangMessages = messages.computeIfAbsent(langCode, (l) -> new HashMap<>());
                    try(InputStream stream = f.getInputStream(e)) {
                      props.load(stream);
                    }
                    for(Map.Entry<Object, Object> entry : props.entrySet()) {
                      String format = (String)entry.getValue();
                      thisLangMessages.put((String) entry.getKey(), ThreadLocal.withInitial(() -> new MessageFormat(format, locale)));
                    }
                  }
                } catch(IOException e) {
                  log.warn("Could not load properties from {}", p);
                }
              }
            });
  }

  public String resolve(StatusMessage msg, Locale locale) {
    Map<String, ThreadLocal<MessageFormat>> formats = null;
    ThreadLocal<MessageFormat> format = null;
    List<String> searchKeys = null;
    if(locale == null) {
      // no locale, just use the base messages
      searchKeys = List.of("");
      log.debug("Looking up {} in base bundle only", msg.getCode());
    } else {
      // first try the full locale tag, then fall back on just the language part
      searchKeys = localeCache.get(locale);
      log.debug("Looking up {} in bundles {}",  msg.getCode(), searchKeys);
    }
    for(String k : searchKeys) {
      formats = messages.get(k);
      if(formats != null) {
        format = formats.get(msg.getCode());
      }
      if(format != null) {
        log.debug("Found {} in bundle {}", msg.getCode(), k);
        break;
      }
    }

    if(format == null) {
      // not found in any of the language lists, so fall back on the message "text"
      format = textCache.get(msg.getText());
      log.debug("Not found, falling back on message text");
    }

    return format.get().format(msg.getParams() == null ? new Object[0] : msg.getParams().toArray());
  }
}
