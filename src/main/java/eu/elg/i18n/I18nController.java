package eu.elg.i18n;

import eu.elg.model.StatusMessage;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.http.HttpHeaders;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.server.util.locale.HttpLocaleResolutionConfiguration;

import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Very simple controller that takes a JSON list of ELG status messages and returns their resolved strings in the locale
 * of the request (as determined by the Accept-Language header).
 */
@Controller
public class I18nController {

  public I18nController(MessageResolver messageResolver, HttpLocaleResolutionConfiguration localeConfig) {
    this.messageResolver = messageResolver;
    this.localeConfig = localeConfig;
  }

  private final MessageResolver messageResolver;
  private final HttpLocaleResolutionConfiguration localeConfig;

  @Post(uri = "/resolve", consumes = {"application/json"})
  public HttpResponse<List<String>> resolveMessages(@Body List<StatusMessage> messages, @Nullable Locale localeFromRequest) {
    final Locale locale = Optional.ofNullable(localeFromRequest).orElseGet(localeConfig::getDefaultLocale);
    return HttpResponse.ok().headers(h -> {
      // add a Content-Language header if we didn't return the default locale
      if(localeFromRequest != null) {
        h.add(HttpHeaders.CONTENT_LANGUAGE, localeFromRequest.toLanguageTag());
      }
    }).body(messages.stream().map((msg) -> messageResolver.resolve(msg, locale)).collect(Collectors.toList()));
  }
}
